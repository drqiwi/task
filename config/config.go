package config

import (
	"encoding/json"
	"log"
	"os"
)

type Config struct {
	ServiceAddress string `json:"service_addr,omitempty"`
	PostgresURL    string `json:"postgres_url,omitempty"`
}

func LoadConfiguration(filename string) (config Config, err error) {
	file, err := os.Open(filename)
	if err != nil {
		return config, err
	}

	defer func() {
		if errorClose := file.Close(); errorClose != nil {
			log.Printf("cannot close file %s: %s", filename, errorClose)
		}
	}()

	jsonParser := json.NewDecoder(file)

	err = jsonParser.Decode(&config)
	if err != nil {
		return config, err
	}

	return config, nil
}
