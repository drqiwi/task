FROM golang:1.16-alpine

WORKDIR /app

COPY ./ /app

RUN go mod download

ENTRYPOINT go run cmd/main.go