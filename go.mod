module gitlab.com/drqiwi/task

go 1.13

require (
	github.com/Microsoft/go-winio v0.5.0 // indirect
	github.com/containerd/continuity v0.1.0 // indirect
	github.com/go-redis/redis/v8 v8.8.2
	github.com/golang/mock v1.5.0
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.1
	github.com/ory/dockertest/v3 v3.6.5
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/stretchr/testify v1.7.0
	golang.org/x/net v0.0.0-20210505214959-0714010a04ed // indirect
	golang.org/x/sys v0.0.0-20210503173754-0981d6026fa6 // indirect
)
