package models

import (
	"errors"

	"gitlab.com/drqiwi/task/driver"
)

type InfoToDB struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

type InfoFromDB struct {
	ID int `json:"id"`
}

func (i InfoToDB) Validate() error {
	if i.Name == "" {
		return errors.New("name can't be empty")
	}

	if i.Age < 10 {
		return errors.New("age can't be smaller than 10")
	}

	return nil
}

func MapInfoToUserData(i InfoToDB) driver.UserData {
	return driver.UserData {
		Name: i.Name,
		Age: i.Age,
	}
}