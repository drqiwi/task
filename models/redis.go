package models

import (
	"errors"

	"gitlab.com/drqiwi/task/driver"
)

type InfoToRedis struct {
	Key   string `json:"key"`
	Value int    `json:"value"`
}

type InfoFromRedis struct {
	Value int `json:"value"`
}

func (i InfoToRedis) Validate() error {
	if i.Key == "" {
		return errors.New("key can't be empty")
	}

	return nil
}

func MapInfoToRedisData(i InfoToRedis) driver.RedisData {
	return driver.RedisData {
		Key: i.Key,
		Value: i.Value,
	}
}