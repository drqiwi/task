package models

import (
	"errors"
)

type HMACRequest struct {
	Text string `json:"text"`
	Key  string `json:"key"`
}

type HMACResponse struct {
	Hmac string `json:"hmac"`
}

func (h HMACRequest) Validate() error {
	if h.Text == "" {
		return errors.New("text can't be empty")
	}

	if h.Key == "" {
		return errors.New("key can't be empty")
	}

	return nil
}
