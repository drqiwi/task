package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/drqiwi/task/config"
	"gitlab.com/drqiwi/task/delivery"
	"gitlab.com/drqiwi/task/driver"

	"github.com/gorilla/mux"
)

const (
	service = "myService"
)

var (
	redisHost       = flag.String("host", "redis", "redis address")
	redisPort       = flag.String("port", "6379", "redis port")
	shutdownTimeout = flag.Duration("timeout", 10*time.Second, "timeout before connections are cancelled")
)

func main() {
	flag.Parse()

	cfg, err := config.LoadConfiguration("config.json")
	if err != nil {
		log.Fatalf("cannot load configuration: %s", err)
	}

	db, closeSQLConnectionFunc, err := driver.ConnectDB(cfg.PostgresURL)
	if err != nil {
		log.Fatalf("cannot set connection to SQL database: %s", err)
	}

	conn, closeRedisConnectionFunc, err := driver.ConnectRedis(*redisHost, *redisPort)
	if err != nil {
		closeSQLConnectionFunc()
		log.Fatalf("cannot set connection to Redis service: %s", err)
	}

	repoSQL := driver.NewSQLRepository(db)
	repoRedis := driver.NewRedisRepository(conn)

	userHandler := delivery.NewHandler(repoSQL, repoRedis)

	router := mux.NewRouter()
	router.HandleFunc("/postgres/users", userHandler.CreateUser).Methods(http.MethodPost)
	router.HandleFunc("/sign/hmacsha512", userHandler.GetHash).Methods(http.MethodPost)
	router.HandleFunc("/redis/incr", userHandler.GetIncrement).Methods(http.MethodPost)

	srv := &http.Server{
		Addr:    cfg.ServiceAddress,
		Handler: router,
	}

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)

	go func() {
		log.Printf("%s listening on %s with %v timeout", service, cfg.ServiceAddress, *shutdownTimeout)
		if err := srv.ListenAndServe(); err != nil {
			if err != http.ErrServerClosed {
				log.Printf("%s failed to listen: %s", service, err)
			}
		}
	}()
	<-stop

	log.Printf("%s shutting down ...\n", service)

	ctx, cancel := context.WithTimeout(context.Background(), *shutdownTimeout)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		log.Printf("%s failed to shutown %s: ", service, err)
	}

	closeSQLConnectionFunc()
	closeRedisConnectionFunc()

	log.Printf("%s down\n", service)
}
