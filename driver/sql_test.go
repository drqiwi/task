package driver

import (
	"database/sql"
	"fmt"
	"log"
	"testing"

	"github.com/ory/dockertest/v3"
	"github.com/stretchr/testify/assert"
)

var userData = UserData{
	Name: "Alexei",
	Age:  25,
}

func TestAddUser(t *testing.T) {
	var db *sql.DB

	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("could not connect to docker: %s", err)
	}

	resource, err := pool.Run("postgres", "9.6", []string{"POSTGRES_PASSWORD=secret", "POSTGRES_DB=postgres"})
	if err != nil {
		log.Fatalf("could not start resource: %s", err)
	}

	if err = pool.Retry(func() error {
		db, err = sql.Open("postgres", fmt.Sprintf("postgres://postgres:secret@localhost:%s/postgres?sslmode=disable", resource.GetPort("5432/tcp")))
		if err != nil {
			return err
		}
		return db.Ping()
	}); err != nil {
		log.Fatalf("could not connect to docker: %s", err)
	}

	_, err = db.Exec("CREATE TABLE users (id serial PRIMARY KEY, name VARCHAR(50), age INT)")
	if err != nil {
		log.Fatalf("could not create database: %s", err)
	}

	repo := NewSQLRepository(db)
	res, err := repo.AddUser(userData)

	assert.NoError(t, err)
	assert.Equal(t, 1, res)

	if err = pool.Purge(resource); err != nil {
		log.Fatalf("could not purge resource: %s", err)
	}
}
