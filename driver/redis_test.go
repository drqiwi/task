package driver

import (
	"context"
	"fmt"
	"log"
	"testing"

	"github.com/go-redis/redis/v8"
	"github.com/ory/dockertest/v3"
	"github.com/stretchr/testify/assert"
)

var redisData = RedisData{
	Key:   "User",
	Value: 25,
}

func TestIncrement(t *testing.T) {
	var conn *redis.Client

	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("could not connect to docker: %s", err)
	}

	resource, err := pool.Run("redis", "alpine", nil)
	if err != nil {
		log.Fatalf("could not start resource: %s", err)
	}

	if err = pool.Retry(func() error {
		conn = redis.NewClient(&redis.Options{
			Addr: fmt.Sprintf("localhost:%s", resource.GetPort("6379/tcp")),
		})
		return conn.Ping(context.TODO()).Err()
	}); err != nil {
		log.Fatalf("could not connect to docker: %s", err)
	}

	repo := NewRedisRepository(conn)
	res, err := repo.Increment(redisData)

	assert.NoError(t, err)
	assert.Equal(t, 26, res)

	if err = pool.Purge(resource); err != nil {
		log.Fatalf("could not purge resource: %s", err)
	}
}
