package driver

import (
	"database/sql"
)

type SQLRepository struct {
	db *sql.DB
}

func NewSQLRepository(db *sql.DB) *SQLRepository {
	return &SQLRepository{
		db: db,
	}
}

func (r SQLRepository) AddUser(u UserData) (int, error) {
	var createdUserID int

	err := r.db.QueryRow("INSERT INTO users (name, age) VALUES($1, $2) RETURNING id", u.Name, u.Age).
		Scan(&createdUserID)
	if err != nil {
		return 0, err
	}

	return createdUserID, nil
}

type UserData struct {
	Name string
	Age  int
}
