package driver

import (
	"github.com/go-redis/redis/v8"
)

type RedisRepository struct {
	rdb *redis.Client
}

func NewRedisRepository(rdb *redis.Client) *RedisRepository {
	return &RedisRepository{
		rdb: rdb,
	}
}

func (r RedisRepository) Increment(dataRedis RedisData) (int, error) {
	err := r.rdb.Set(ctx, dataRedis.Key, dataRedis.Value, 0).Err()
	if err != nil {
		return 0, err
	}

	res, err := r.rdb.Incr(ctx, dataRedis.Key).Result()
	if err != nil {
		return 0, err
	}

	return int(res), nil
}

type RedisData struct {
	Key   string
	Value int
}
