package driver

import (
	"context"
	"database/sql"
	"log"

	"github.com/go-redis/redis/v8"
	"github.com/lib/pq"
)

var (
	ctx = context.TODO()
)

func ConnectDB(connectionString string) (*sql.DB, func(), error) {
	parsedConnectionString, err := pq.ParseURL(connectionString)
	if err != nil {
		return nil, nil, err
	}

	db, err := sql.Open("postgres", parsedConnectionString)
	if err != nil {
		return nil, nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, nil, err
	}

	return db, func() {
		if errorClose := db.Close(); errorClose != nil {
			log.Printf("cannot close postgres connection: %s", errorClose)
		}
	}, nil
}

func ConnectRedis(host string, port string) (*redis.Client, func(), error) {
	rdb := redis.NewClient(&redis.Options{
		Addr:     host + ":" + port,
		Password: "",
		DB:       0,
	})

	_, err := rdb.Ping(ctx).Result()
	if err != nil {
		return nil, nil, err
	}

	return rdb, func() {
		if errorClose := rdb.Close(); errorClose != nil {
			log.Printf("cannot close redis connection: %s", errorClose)
		}
	}, nil
}
