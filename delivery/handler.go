package delivery

import (
	"encoding/json"
	"net/http"

	"gitlab.com/drqiwi/task/driver"
	"gitlab.com/drqiwi/task/models"
	"gitlab.com/drqiwi/task/repository"
)

type SQLRepo interface {
	AddUser(u driver.UserData) (int, error)
}

type RedisRepo interface {
	Increment(u driver.RedisData) (int, error)
}

type Handler struct {
	repoSQL   SQLRepo
	repoRedis RedisRepo
}

func NewHandler(repoSQL SQLRepo, repoRedis RedisRepo) *Handler {
	return &Handler{
		repoSQL:   repoSQL,
		repoRedis: repoRedis,
	}
}

func (h Handler) CreateUser(w http.ResponseWriter, r *http.Request) {
	userInfo := models.InfoToDB{}

	err := json.NewDecoder(r.Body).Decode(&userInfo)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := userInfo.Validate(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	createdUserID, err := h.repoSQL.AddUser(models.MapInfoToUserData(userInfo))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	response, err := json.Marshal(models.InfoFromDB{ID: createdUserID})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	_, err = w.Write(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h Handler) GetHash(w http.ResponseWriter, r *http.Request) {
	request := models.HMACRequest{}

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := request.Validate(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	hmac, err := repository.ComputeHash(request.Text, request.Key)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	responseBytes, err := json.Marshal(models.HMACResponse{Hmac: hmac})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	_, err = w.Write(responseBytes)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h Handler) GetIncrement(w http.ResponseWriter, r *http.Request) {
	redisInfo := models.InfoToRedis{}

	err := json.NewDecoder(r.Body).Decode(&redisInfo)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := redisInfo.Validate(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	increment, err := h.repoRedis.Increment(models.MapInfoToRedisData(redisInfo))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	response, err := json.Marshal(models.InfoFromRedis{Value: increment})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	_, err = w.Write(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
