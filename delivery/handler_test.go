//mockgen -source=handler.go -destination=handler_mock.go -package=delivery
package delivery

import (
	"testing"

	"gitlab.com/drqiwi/task/driver"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestCreateUser(t *testing.T) {
	var userData = driver.UserData{
		Name: "Alex",
		Age:  25,
	}

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockObj := NewMockSQLRepo(mockCtrl)
	mockObj.EXPECT().AddUser(userData).Return(1, nil)

	handler := NewHandler(mockObj, nil)
	res, err := handler.repoSQL.AddUser(userData)

	assert.NoError(t, err)
	assert.Equal(t, 1, res)
}

func TestGetIncrement(t *testing.T) {
	var redisData = driver.RedisData{
		Key:   "User",
		Value: 25,
	}

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockObj := NewMockRedisRepo(mockCtrl)
	mockObj.EXPECT().Increment(redisData).Return(26, nil)

	handler := NewHandler(nil, mockObj)
	res, err := handler.repoRedis.Increment(redisData)

	assert.NoError(t, err)
	assert.Equal(t, 26, res)
}
