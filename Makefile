.PHONY: help build up start down destroy stop restart logs logs-app ps

help: ## Displays this help
	@awk 'BEGIN {FS = ":.*##"; printf "Usage:\n  make \033[36m\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)
build: ## Builds image from Dockerfile
	docker-compose -f docker-compose.yml build
up: ## Builds, (re)creates, starts, and attaches to containers
	docker-compose -f docker-compose.yml up -d
start: ## Starts existing containers
	docker-compose -f docker-compose.yml start
down: ## Stops containers and removes containers
	docker-compose -f docker-compose.yml down
stop: ## Stops running containers without removing them
	docker-compose -f docker-compose.yml stop
restart: ## Restarts containers
	docker-compose -f docker-compose.yml stop
	docker-compose -f docker-compose.yml up -d
logs: ## Displays logs for containers
	docker-compose -f docker-compose.yml logs --tail=100 -f
logs-app: ## Displays logs for app
	docker-compose -f docker-compose.yml logs --tail=100 -f app
ps: ## Lists containers
	docker-compose -f docker-compose.yml ps