CREATE TABLE IF NOT EXISTS users (
    id serial NOT NULL,
    name varchar(50) NOT NULL,
    age INT NOT NULL,
    PRIMARY KEY (id)
);