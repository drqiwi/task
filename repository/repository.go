package repository

import (
	"crypto/hmac"
	"crypto/sha512"
	"encoding/hex"
)

func ComputeHash(text, key string) (string, error) {
	var sha string

	h := hmac.New(sha512.New, []byte(key))

	_, err := h.Write([]byte(text))
	if err != nil {
		return sha, err
	}

	sha = hex.EncodeToString(h.Sum(nil))

	return sha, nil
}
