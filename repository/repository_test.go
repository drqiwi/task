package repository

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestComputeHash(t *testing.T) {
	testCases := map[string]struct {
		text     string
		key      string
		expected string
	}{
		"test normal": {
			text:     "text",
			key:      "test123",
			expected: "564eb5cf2a2933a96e61840f716832fceeee363eb054b25a0be9a88c392822f417bbdb418c3c7fc23a3770840d91b2e6933b4056da787b43743a8fcd779fb4c3",
		},
		"text is empty": {
			text:     "",
			key:      "test123",
			expected: "7d5df84243730879ddb7a2d6e7d81154ae3d28ba950d49fa0b162698bf7fb144534826fa1cba560ffa6e68967996459d49584caf7b87cde2a109fc76bf8b881d",
		},
		"key is empty": {
			text:     "text",
			key:      "",
			expected: "6c7e747546d1955884a4da76925b48c89334ad512a535587d800690908270ef468b920e014f435887b16f54005d3f644a3592ea7197e250f8b5fcea7afb06923",
		},
		"both are empty": {
			text:     "",
			key:      "",
			expected: "b936cee86c9f87aa5d3c6f2e84cb5a4239a5fe50480a6ec66b70ab5b1f4ac6730c6c515421b327ec1d69402e53dfb49ad7381eb067b338fd7b0cb22247225d47",
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			res, err := ComputeHash(tc.text, tc.key)

			assert.NoError(t, err)
			assert.Equal(t, tc.expected, res)
		})
	}
}
